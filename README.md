# Screen Size Test

Resize in realtime to test pages in other sizes.

[DEMO](https://dartpad.dartlang.org/43d9c47a8bf031ce3ef2f6314c9dbd52)

<img src="https://gitlab.com/jorgesanure-pub-dep/screen-size-test/-/raw/master/assets/demo.gif" height='300px'>

```dart
import 'package:screen_size_test/screen_size_test.dart';
...
MaterialApp(
  title: 'Demo',
  builder: (context, child) => ScreenSizeTest(
    child: child,
  ),
  home: Scaffold(
    body: ListView(
      children: List.generate(
          20,
          (index) => Container(
                padding: EdgeInsets.all(10),
                child: Placeholder(),
              )),
    ),
  ),
)
```