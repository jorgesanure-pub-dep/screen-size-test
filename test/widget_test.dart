// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/widgets.dart';

import 'package:screen_size_test/main.dart';
import 'package:screen_size_test/screen_size_cases/case_a.dart';

void main() {
  // testWidgets('Counter increments smoke test', (WidgetTester tester) async {
  //   // Build our app and trigger a frame.
  //   await tester.pumpWidget(MyApp());

  //   // Verify that our counter starts at 0.
  //   expect(find.text('0'), findsOneWidget);
  //   expect(find.text('1'), findsNothing);

  //   // Tap the '+' icon and trigger a frame.
  //   await tester.tap(find.byIcon(Icons.add));
  //   await tester.pump();

  //   // Verify that our counter has incremented.
  //   expect(find.text('0'), findsNothing);
  //   expect(find.text('1'), findsOneWidget);
  // });

  // testWidgets("foo", (tester) async {
  //   tester.binding.window.physicalSizeTestValue = Size(500, 742);

  //   // resets the screen to its orinal size after the test end
  //   addTearDown(tester.binding.window.clearPhysicalSizeTestValue);
  //   await tester.pumpWidget(MyApp());
  // });

  testWidgets("case a", (tester) async {
    tester.binding.window.physicalSizeTestValue = Size(500, 742);

    // resets the screen to its orinal size after the test end
    addTearDown(tester.binding.window.clearPhysicalSizeTestValue);

    try {
      final x = await tester.pumpWidget(CaseA());
    } catch (e) {
      print('=================> e');
      print(e);
    }
  });
}
