import 'package:flutter/material.dart';
import 'package:screen_size_test/screen_size_test.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'Demo',
        builder: (context, child) => ScreenSizeTest(
          child: child,
        ),
        home: Scaffold(
          body: ListView(
            children: List.generate(
                6,
                (index) => Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Text(
                            'Veniam cillum esse ex aliqua nostrud cupidatat quis laborum. Aliqua nisi elit consectetur qui cillum magna ea exercitation id. Exercitation quis id cillum elit amet ut non laborum.',
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          SizedBox(height: 150, child: Placeholder()),
                        ],
                      ),
                    )),
          ),
        ),
      );
}
