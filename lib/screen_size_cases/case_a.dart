import 'package:flutter/material.dart';

class CaseA extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        home: Scaffold(
          body: Row(
            children: [
              Container(
                height: 100,
                width: 600,
                color: Colors.red,
              )
            ],
          ),
        ),
      );
}
