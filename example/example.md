```dart
import 'package:screen_size_test/screen_size_test.dart';
...
MaterialApp(
  title: 'Demo',
  builder: (context, child) => ScreenSizeTest(
    child: child,
  ),
  home: Scaffold(
    body: ListView(
      children: List.generate(
          20,
          (index) => Container(
                padding: EdgeInsets.all(10),
                child: Placeholder(),
              )),
    ),
  ),
)
```